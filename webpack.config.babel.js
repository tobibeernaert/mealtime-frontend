import webpack from 'webpack';
import path from 'path';
import fs from 'fs';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const JS_FOLDER_PATH = './src';
const JS_INPUT_PATH = `${JS_FOLDER_PATH}/index.js`;
const JS_OUTPUT_PATH = 'main.js';
const HTML_TEMPLATE_PATH = './src/index.html';
const CSS_OUTPUT_PATH = 'main.css';
const CSS_LOADER = 'css-loader?modules&importLoaders=0&localIdentName=[name]__[local]___[hash:base64:5]';

const createConfig = () => {
  return {
    entry: {
      script: [JS_INPUT_PATH],
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      // publicPath: environments[env].publicPath,
      filename: JS_OUTPUT_PATH,
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          include: path.resolve(__dirname, 'src'),
          use: ['babel-loader', 'eslint-loader'],
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            fallback: ['style-loader'],
            use: [CSS_LOADER, 'postcss-loader'],
          }),
        },
      ],
    },
    plugins: [
      new ExtractTextPlugin({
        filename: CSS_OUTPUT_PATH,
        allChunks: true,
        disable: true,
      }),
      new HtmlWebpackPlugin({
        template: HTML_TEMPLATE_PATH,
      }),
    ],
    devtool: 'source-map',
    devServer: {
      port: process.env.PORT || 3004,
      host: 'mealtime.magier.be',
      publicPath: '/',
      contentBase: './dist',
      historyApiFallback: true,
    },
  };
};

module.exports = createConfig;
