import React from 'react';
import styles from './meal.css';
// import { Card, CardTitle } from 'react-materialize';
import propTypes from 'prop-types';
import Card from '../Card';

const Meal = ({ recipe }) => {
  return (
    <div className={'row ' + styles.meal}>
      <div className="col s12">
        <Card recipe={recipe} />
      </div>
    </div>
  );
};

Meal.propTypes = {
  recipe: propTypes.object,
};

export default Meal;
