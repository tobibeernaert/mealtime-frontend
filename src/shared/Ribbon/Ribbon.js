import React from 'react';
import styles from './ribbon.css';
import propTypes from 'prop-types';

const Ribbon = ({ ribbonContent }) => {
  return (
    <div className={styles.ribbon}>
      <span className={styles['ribbon-content']}>
        <b>{ribbonContent}</b>
      </span>
    </div>
  );
};

Ribbon.propTypes = {
  ribbonContent: propTypes.string,
};

export default Ribbon;
