import React from 'react';
import PropTypes from 'prop-types';

class FormField extends React.Component {
  static propTypes = {
    children: PropTypes.node,
    id: PropTypes.string,
    label: PropTypes.string,
    classNames: PropTypes.string,
  };

  render() {
    const { children, id, label, classNames } = this.props;
    return (
      <div className={'input-field col' + classNames}>
        {children}
        <label htmlFor={id}>{label}</label>
      </div>
    );
  }
}

export default FormField;
