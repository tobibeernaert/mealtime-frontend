import React from 'react';
import propTypes from 'prop-types';

const Card = ({ recipe }) => {
  return (
    <div className="card">
      <div className="card-image">
        <img src="https://d2lm6fxwu08ot6.cloudfront.net/img-thumbs/960w/PD0XWZZSX5.jpg" />
        <span className="card-title white-text">{recipe.name}</span>
        <a className="btn-floating btn-large halfway-fab waves-effect waves-light teal" href="#">
          <i className="material-icons">restaurant_menu</i>
        </a>
      </div>
      <div className="card-content">
        <p />
      </div>
    </div>
  );
};

Card.propTypes = {
  recipe: propTypes.object,
};

export default Card;
