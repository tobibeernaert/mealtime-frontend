import React from 'react';
// import ModalContent from './components/ModalContent';
// import ModalFooter from './components/ModalFooter';
import PropTypes from 'prop-types';
// import styles from './modal.css';

class Modal extends React.Component {
  static propTypes = {
    identifier: PropTypes.string,
    isOpen: PropTypes.bool.isRequired,
    children: PropTypes.node,
  };

  constructor(props) {
    super(props);

    this.state = { isOpen: false };
    this.close = this.close.bind(this);
  }

  componentDidMount() {
    // Focus needs to be set when mounting and already open
    if (this.props.isOpen) {
      // this.setFocusAfterRender(true);
      this.open();
    }
  }

  componentWillReceiveProps(newProps) {
    // Focus only needs to be set once when the modal is being opened
    if (!this.props.isOpen && newProps.isOpen) {
      // this.setFocusAfterRender(true);
      this.open();
    } else if (this.props.isOpen && !newProps.isOpen) {
      this.close();
    }
  }

  open = () => {
    this.setState({ isOpen: true });
  };

  close = () => {
    this.setState({ isOpen: false });
  };

  renderStyles = () => {
    let style = {};
    if (this.state.isOpen) {
      style = { display: 'block', opacity: 1, bottom: '0px', zIndex: 1001 };
    }

    return style;
  };

  renderOverlayStyles = () => {
    let style = {};
    if (this.state.isOpen) {
      style = { display: 'block', opacity: 0.5, zIndex: 999 };
    }

    return style;
  };

  render() {
    return (
      <div>
        <div className="modal-overlay" style={this.renderOverlayStyles()} />
        <div id={this.props.identifier} style={this.renderStyles()} className="modal bottom-sheet">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Modal;
