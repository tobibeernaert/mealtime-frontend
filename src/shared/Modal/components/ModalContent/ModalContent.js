import React from 'react';
import propTypes from 'prop-types';

const ModalContent = ({ header, text }) => {
  return (
    <div className="modal-content">
      <h4>{header}</h4>
      <p>{text}</p>
    </div>
  );
};

ModalContent.propTypes = {
  header: propTypes.string,
  text: propTypes.string,
};

export default ModalContent;
