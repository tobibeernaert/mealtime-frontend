import React from 'react';
import PropTypes from 'prop-types';

class ModalFooter extends React.Component {
  static propTypes = {
    onAgree: PropTypes.func,
    onCancel: PropTypes.func,
    // onClose: PropTypes.func,
  };

  render = () => {
    return (
      <div className="modal-footer">
        <a
          href="#!"
          className="left modal-action modal-close waves-effect waves-green btn-flat red lighten-2"
          onClick={this.props.onCancel}
        >
          Cancel
        </a>
        &nbsp;
        <a
          href="#!"
          className="modal-action modal-close waves-effect waves-green btn-flat blue lighten-2"
          onClick={this.props.onAgree}
        >
          Agree
        </a>
      </div>
    );
  };
}

export default ModalFooter;
