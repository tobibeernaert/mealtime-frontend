import React, { Component } from 'react';
import MealList from '../MealList';

class Tomorrow extends Component {
  constructor(props) {
    super(props);

    this.state = { meals: [] };
  }

  componentDidMount() {
    this.fetchMeals();
  }

  fetchMeals() {
    fetch('https://mealtime.magier.be:4333/api/meals/tomorrow')
      .then(result => result.json())
      .then(meals => this.setState({ meals: meals.data }));
  }

  render() {
    return (
      <div>
        <h1>Tomorrow</h1>
        <MealList meals={this.state.meals} />
      </div>
    );
  }
}

export default Tomorrow;
