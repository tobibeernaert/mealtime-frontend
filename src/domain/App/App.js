import React, { Component } from 'react';
import Header from './Header';
import Routes from '../Route/Routes';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Routes />
        </div>
      </div>
    );
  }
}

export default App;
