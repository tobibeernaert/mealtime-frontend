import React, { Component } from 'react';
import { Navbar, NavItem, Icon } from 'react-materialize';

class Header extends Component {
  render() {
    return (
      <Navbar brand={<Icon large>restaurant</Icon>} className="teal lighten-2" right>
        <NavItem href="/">Today</NavItem>
        <NavItem href="/tomorrow">Tomorrow</NavItem>
        <NavItem href="/admin">
          <i className="material-icons">lock</i>
        </NavItem>
      </Navbar>
    );
  }
}

export default Header;
