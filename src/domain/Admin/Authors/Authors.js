import React from 'react';
import Modal from '../../../shared/Modal';
import ModalContent from '../../../shared/Modal/components/ModalContent';
import ModalFooter from '../../../shared/Modal/components/ModalFooter';
import { Link } from 'react-router-dom';

class Authors extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      authors: [],
      modalIsOpen: false,
      needsUpdate: false,
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.onRequestAgree = this.onRequestAgree.bind(this);
    this.modalContentHeader = '';
    this.modalContentDescription = '';
  }

  componentWillMount() {
    this.AuthorsList();
  }

  componentDidMount() {
    this.AuthorsList();
  }

  componentWillReceiveProps(newProps) {
    if (this.state.needsUpdate) {
      this.setState({ needsUpdate: false });
      this.AuthorsList();
    }
  }

  AuthorsList() {
    fetch('https://mealtime.magier.be:4333/api/authors')
      .then(result => result.json())
      .then(authors => this.setState({ authors: authors.data }));
  }

  deleteAuthor = author => {
    fetch('https://mealtime.magier.be:4333/api/authors/' + author.id, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => {
      if (response.status === 200) {
        const newState = this.state.authors;
        if (newState.indexOf(author) > -1) {
          newState.splice(newState.indexOf(author), 1);
          this.setState({ authors: newState });
        }
      } else {
        // TODO erro handling
        console.log('Could not delete author ' + author.id);
      }
    });
  };

  openModal = author => {
    event.preventDefault();
    this.setState({
      modalIsOpen: true,
      authorToDelete: author,
    });

    this.modalContentHeader = 'Delete ' + author.name;
    this.modalContentDescription = 'Do you want to delete ' + author.name + '?';
  };

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  onRequestAgree = event => {
    this.setState({ modalIsOpen: false });
    this.deleteAuthor(this.state.authorToDelete);
  };

  render() {
    return (
      <div>
        <h1>Authors</h1>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Website</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.state.authors.map(function(author, index) {
              return (
                <tr key={author.id}>
                  <td>{author.name}</td>
                  <td>{author.website}</td>
                  <td>
                    <a href="#edit">
                      <i className="material-icons">edit</i>
                    </a>
                    <a href="#delete" onClick={event => this.openModal(author)}>
                      <i className="material-icons red-text">delete</i>
                    </a>
                  </td>
                </tr>
              );
            }, this)}
          </tbody>
        </table>
        <Link className="waves-effect waves-light btn right" to="/admin-authors-create">
          <i className="material-icons right">add</i>
          Add Author
        </Link>
        <Modal isOpen={this.state.modalIsOpen} author={this.state.authorToDelete}>
          <ModalContent header={this.modalContentHeader} text={this.modalContentDescription} />
          <ModalFooter onAgree={event => this.onRequestAgree()} onCancel={this.closeModal} />
        </Modal>
      </div>
    );
  }
}

export default Authors;
