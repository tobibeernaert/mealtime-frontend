import React from 'react';
import Form from '../../../../shared/Form';
import FormField from '../../../../shared/FormField';
import { Redirect } from 'react-router';

class CreateForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fireRedirect: false,
    };
  }

  createAuthor = event => {
    event.preventDefault();

    fetch('https://mealtime.magier.be:4333/api/authors', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: this.name.value,
        website: this.website.value,
      }),
    }).then(result => {
      if (result.status === 204) {
        this.setState({ fireRedirect: true });
      } else {
        console.log('something went wrong');
      }
    });
  };

  wasFormPosted = () => {
    return this.state.fireRedirect;
  };

  render() {
    return this.wasFormPosted() ? (
      <Redirect to={'/admin-authors'} />
    ) : (
      <Form onSubmit={this.createAuthor}>
        <FormField id="name" label="Name">
          <input
            ref={ref => {
              this.name = ref;
            }}
            placeholder="Name"
            id="name"
            type="text"
            className="validate"
          />
        </FormField>
        <FormField id="website" label="Website">
          <input
            ref={ref => {
              this.website = ref;
            }}
            placeholder="http://"
            id="website"
            type="url"
            className="validate"
          />
        </FormField>
        <button className="btn waves-effect waves-light" type="submit">
          Submit
          <i className="material-icons right">send</i>
        </button>
      </Form>
    );
  }
}

export default CreateForm;
