import React from 'react';
import Modal from '../../../shared/Modal';
import ModalContent from '../../../shared/Modal/components/ModalContent';
import ModalFooter from '../../../shared/Modal/components/ModalFooter';
import { Link } from 'react-router-dom';

class Recipes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      recipes: [],
      modalIsOpen: false,
      needsUpdate: false,
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.onRequestAgree = this.onRequestAgree.bind(this);
    this.modalContentHeader = '';
    this.modalContentDescription = '';
  }

  componentWillMount() {
    this.RecipesList();
  }

  componentDidMount() {
    this.RecipesList();
  }

  componentWillReceiveProps(newProps) {
    if (this.state.needsUpdate) {
      this.setState({ needsUpdate: false });
      this.RecipesList();
    }
  }

  RecipesList() {
    fetch('https://mealtime.magier.be:4333/api/recipes')
      .then(result => result.json())
      .then(recipes => this.setState({ recipes: recipes.data }));
  }

  deleteRecipe = recipe => {
    fetch('https://mealtime.magier.be:4333/api/recipes/' + recipe.id, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => {
      if (response.status === 200) {
        const newState = this.state.recipes;
        if (newState.indexOf(recipe) > -1) {
          newState.splice(newState.indexOf(recipe), 1);
          this.setState({ recipes: newState });
        }
      } else {
        // TODO erro handling
        console.log('Could not delete recipe ' + recipe.id);
      }
    });
  };

  openModal = recipe => {
    event.preventDefault();
    this.setState({
      modalIsOpen: true,
      recipeToDelete: recipe,
    });

    this.modalContentHeader = 'Delete ' + recipe.name;
    this.modalContentDescription = 'Do you want to delete ' + recipe.name + '?';
  };

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  onRequestAgree = event => {
    this.setState({ modalIsOpen: false });
    this.deleteRecipe(this.state.recipeToDelete);
  };

  render() {
    return (
      <div>
        <h1>Recipes</h1>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Author</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {this.state.recipes.map(function(recipe, index) {
              return (
                <tr key={recipe.id}>
                  <td>{recipe.name}</td>
                  <td>
                    <a href="#">{recipe.author.data.name}</a>
                  </td>
                  <td>
                    <a href="#edit">
                      <i className="material-icons">edit</i>
                    </a>
                    <a href="#delete" onClick={event => this.openModal(recipe)}>
                      <i className="material-icons red-text">delete</i>
                    </a>
                  </td>
                </tr>
              );
            }, this)}
          </tbody>
        </table>
        <Link className="waves-effect waves-light btn right" to="/admin-recipes-create">
          <i className="material-icons right">add</i>
          Add Recipe
        </Link>
        <Modal isOpen={this.state.modalIsOpen} recipe={this.state.recipeToDelete}>
          <ModalContent header={this.modalContentHeader} text={this.modalContentDescription} />
          <ModalFooter onAgree={event => this.onRequestAgree()} onCancel={this.closeModal} />
        </Modal>
      </div>
    );
  }
}

export default Recipes;
