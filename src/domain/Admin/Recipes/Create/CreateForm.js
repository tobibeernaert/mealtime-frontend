import React from 'react';
import Form from '../../../../shared/Form';
import FormField from '../../../../shared/FormField';
import { Redirect } from 'react-router';
import AuthorAutoComplete from './AuthorAutoComplete';

class CreateForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fireRedirect: false,
    };
  }

  createRecipe = event => {
    event.preventDefault();
    console.log('todo');
  };

  wasFormPosted = () => {
    return this.state.fireRedirect;
  };

  render() {
    return this.wasFormPosted() ? (
      <Redirect to={'/admin-recipes'} />
    ) : (
      <Form onSubmit={this.createRecipe}>
        <FormField id="name" label="Name">
          <input
            ref={ref => {
              this.name = ref;
            }}
            placeholder="Name"
            id="name"
            type="text"
            className="validate"
          />
        </FormField>
        <FormField id="author" label="author">
          <AuthorAutoComplete />
        </FormField>
        <br />
        <button className="btn waves-effect waves-light" type="submit">
          Submit
          <i className="material-icons right">send</i>
        </button>
      </Form>
    );
  }
}

export default CreateForm;
