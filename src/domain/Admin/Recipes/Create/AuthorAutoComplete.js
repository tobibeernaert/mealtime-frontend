import React from 'react';
import ReactAutocomplete from 'react-autocomplete';
import styles from './AuthorAutoComplete.css';

class AuthorAutoComplete extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      authors: [],
      filteredAuthors: [],
    };
  }

  componentWillMount() {
    this.getAuthors();
  }

  componentDidMount() {
    this.getAuthors();
  }

  getAuthors() {
    fetch('https://mealtime.magier.be:4333/api/authors')
      .then(result => result.json())
      .then(authors => this.setState({ authors: authors.data, filteredAuthors: authors.data }));
  }

  filterAuthors(value, authorsList, items) {
    return setTimeout(
      items,
      500,
      value ? authorsList.filter(author => this.matchAuthorToTerm(author, value)) : authorsList,
    );
  }

  matchAuthorToTerm(author, value) {
    return author.name.toLowerCase().indexOf(value.toLowerCase()) !== -1;
  }

  render() {
    return (
      <ReactAutocomplete
        inputProps={{ id: 'author' }}
        wrapperStyle={{ position: 'relative', display: 'block' }}
        wrapperProps={{ className: 'autocomplete' }}
        getItemValue={item => item.name}
        items={this.state.filteredAuthors}
        value={this.state.value}
        onSelect={(value, item) => {
          this.setState({ value: value });
          this.setState({ filteredAuthors: this.state.authors });
        }}
        onChange={(event, value) => {
          this.setState({ value: value });
          clearTimeout(this.requestTimer);
          this.requestTimer = this.filterAuthors(value, this.state.authors, items => {
            this.setState({ filteredAuthors: items });
          });
        }}
        renderMenu={children => <ul className="autocomplete-content dropdown-content">{children}</ul>}
        renderItem={(item, isHighlighted) => (
          <li className={`${styles.item} ${isHighlighted ? styles['item-highlighted'] : ''}`} key={item.id}>
            <span>{item.name}</span>
          </li>
        )}
      />
    );
  }
}

export default AuthorAutoComplete;
