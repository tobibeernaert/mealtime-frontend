import React from 'react';
import CreateForm from './CreateForm';
import { Link } from 'react-router-dom';

class Create extends React.Component {
  render() {
    return (
      <div>
        <h1>Add Recipe</h1>
        <CreateForm />
        <Link className="waves-effect waves-light btn left red" to="/admin-recipes">
          <i className="material-icons right">cancel</i>
          Cancel
        </Link>
      </div>
    );
  }
}

export default Create;
