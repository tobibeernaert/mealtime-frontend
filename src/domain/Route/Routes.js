import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Today from './../Today';
import Tomorrow from './../Tomorrow';
import Dashboard from './../Admin/Dashboard';
import Authors from './../Admin/Authors';
import AuthorCreate from './../Admin/Authors/Create';
import Recipes from './../Admin/Recipes';
import RecipeCreate from './../Admin/Recipes/Create';
import NotFound from './../App/NotFound';

const Routes = () => {
  return (
    <Switch>
      <Route exact path={`/`} component={Today} />
      <Route exact path={`/tomorrow`} component={Tomorrow} />
      <Route exact path={`/admin`} component={Dashboard} />
      <Route exact path={`/admin-authors`} component={Authors} />
      <Route exact path={`/admin-authors-create`} component={AuthorCreate} />
      <Route exact path={`/admin-recipes`} component={Recipes} />
      <Route exact path={`/admin-recipes-create`} component={RecipeCreate} />
      <Route component={NotFound} />
    </Switch>
  );
};

export default Routes;
