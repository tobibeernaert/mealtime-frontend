import React from 'react';
import MealList from '../MealList';

class Today extends React.Component {
  constructor() {
    super();

    this.state = { meals: [] };
  }

  componentDidMount() {
    this.fetchMeals();
  }

  fetchMeals() {
    fetch('https://mealtime.magier.be:4333/api/meals/today')
      .then(result => result.json())
      .then(meals => this.setState({ meals: meals.data }));
  }

  render() {
    return (
      <div>
        <h1>Today</h1>
        <MealList meals={this.state.meals} />
      </div>
    );
  }
}

export default Today;
