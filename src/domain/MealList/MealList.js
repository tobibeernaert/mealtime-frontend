import React from 'react';
import Meal from '../../shared/Meal';

const MealList = props => props.meals.map(meal => <Meal key={meal.id} recipe={meal} />);

export default MealList;
