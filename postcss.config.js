module.exports = {
  plugins: [
    require('postcss-pseudoelements')(),
    require('postcss-color-function')(),
    require('postcss-import')({
      root: './',
      path: 'src',
    }),
    require('postcss-cssnext')(),
    require('postcss-nested')(),
    require('postcss-mixins')(),
    require('postcss-each')(),
    require('postcss-extend')(),
    require('postcss-reporter')({
      clearMessages: true,
    }),
  ],
};
